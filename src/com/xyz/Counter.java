package com.xyz;

import java.io.*;
import java.util.*;

public class Counter {
    private File file;
    private Scanner scanner;
    private int numberOfWords = 0;
    private int numberOfChars = 0;
    private ArrayList<String> words = new ArrayList<String>();
    private ArrayList<Character> chars = new ArrayList<Character>();

    public Counter(File file){
        this.file = file;
    }

    public int countWords() throws FileNotFoundException {
        String temp;
        scanner = new Scanner(new FileInputStream("test.txt"));
        while(scanner.hasNext()){
            temp = scanner.next();
            numberOfWords++;
            words.add(temp);
        }
        return numberOfWords;
    }

    public int countCharacters() throws FileNotFoundException {
        scanner = new Scanner(new FileInputStream("test.txt"));
        String wholeText = new String();
        String temp;
        while (scanner.hasNext()){
            temp = scanner.next();
            wholeText = wholeText+temp;
        }
        for (int i = 0; i < wholeText.length(); i++){
            chars.add(wholeText.charAt(i));
        }
        numberOfChars = wholeText.length();
        return numberOfChars;
    }

    private void sort(int[][] a, int l){
        boolean needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < l; i++) {
                if (a[i][1] < a[i - 1][1]) {
                    int temp = a[i][1];
                    a[i][1] = a[i-1][1];
                    a[i-1][1] = temp;
                    temp = a[1][0];
                    a[i][0] = a[i-1][0];
                    a[i-1][0] = temp;
                    needIteration = true;
                }
            }
        }
    }

    public void detailWords() {
        ArrayList<String> uniqueWords = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            boolean isUnique = true;
            for (int j = 0; j < uniqueWords.size(); j++) {
                if (words.get(i).compareTo(uniqueWords.get(j))==0) {
                    isUnique = false;
                }
            }
            if (isUnique) {
                uniqueWords.add(words.get(i));
            }
        }
        int[][] repeats = new int[uniqueWords.size()][2];
        for (int i = 0; i < uniqueWords.size(); i++) {
            String temp = uniqueWords.get(i);
            repeats[i][0] = i;
            for (int j = 0; j < words.size(); j++) {
                if (words.get(j).compareTo(uniqueWords.get(i))==0) {
                    repeats[i][1]++;
                }
            }
        }
        sort(repeats, uniqueWords.size());
        System.out.println("Most recent words (10 or less): ");
        if (uniqueWords.size() < 10) {
            for (int i = 0; i < uniqueWords.size(); i++) {
                System.out.println(uniqueWords.get(repeats[i][0]));
            }
        } else{
            for (int i = 0; i < 10; i++){
                System.out.println(uniqueWords.get(repeats[i][0]));
            }
        }
    }

    public void detailCharacters(){
        ArrayList<Character> uniqueCharacters = new ArrayList<>();
        for (int i = 0; i < chars.size(); i++){
            boolean isUnique = true;
            for(int j = 0; j < uniqueCharacters.size(); j++){
                if (chars.get(i)==uniqueCharacters.get(j)){
                    isUnique = false;
                }
            }
            if (isUnique){
                uniqueCharacters.add(chars.get(i));
            }
        }
        int[][] repeats = new int[uniqueCharacters.size()][2];
        for (int i = 0; i < uniqueCharacters.size(); i++){
            char temp = uniqueCharacters.get(i);
            repeats[i][0] = i;
            for(int j = 0; j < chars.size(); j++){
                if (chars.get(j)==temp){
                    repeats[i][1]++;
                }
            }
        }
        sort(repeats, uniqueCharacters.size());
        System.out.println("Most recent characters (10 or less): ");
        if (uniqueCharacters.size()<10){
            for (int i = 0; i < uniqueCharacters.size(); i++){
                System.out.println(uniqueCharacters.get(repeats[i][0]));
            }
        } else {
            for(int i = 0; 0 < 10; i++){
                System.out.println(uniqueCharacters.get(repeats[i][0]));
            }
        }

    }

}
